
#include "BioEnemyShip.h"


BioEnemyShip::BioEnemyShip()
{
	SetSpeed(150);
	SetMaxHitPoints(1);
	SetCollisionRadius(20);
}


void BioEnemyShip::Update(const GameTime *pGameTime)
{
	if (IsActive()) //determine if the ship is active and on screen
	{
		float x = sin(pGameTime->GetTotalTime() * Math::PI + GetIndex());
		x *= GetSpeed() * pGameTime->GetTimeElapsed() * 1.4f; //calculate the speed of ships on screen
		TranslatePosition(x, GetSpeed() * pGameTime->GetTimeElapsed()); //move BioShip on screen

		if (!IsOnScreen()) Deactivate(); // deactivate the ship if no longer on screen
	}
	 // do any updates that a normal ship would do.
	 // (fire weapons, collide with objects, etc.)
	EnemyShip::Update(pGameTime);
}


void BioEnemyShip::Draw(SpriteBatch *pSpriteBatch)
{
	if (IsActive())
	{
		pSpriteBatch->Draw(m_pTexture, GetPosition(), Color::White, m_pTexture->GetCenter(), Vector2::ONE, Math::PI, 1);
	}
}
